---
home: true
heroAlt: OpenInfra Labs
heroText: OpenInfra Labs
tagline: An <a href="https://osf.dev">OSF</a> Project
featuresTitle: Delivering open source tools to run cloud, container, AI, machine learning and edge workloads repeatedly and predictably
features:
- title: Integrated testing
  image: ./images/features/test.svg
  details: Integrated testing of all the necessary to provide a complete use case
- title: Documentation
  details: Documentation of operational and funcional gaps required to run upstream projects in a production environment
  image: ./images/features/docs.svg
- title: Share repositories
  details: Shared code repositories for operational tooling and the "glue" code that is often written indenpently by users
  image: ./images/features/code.svg
sponsorsTitle: Participants & Supporters
sponsors:
- logo: ./images/sponsors/boston-university.png
- logo: ./images/sponsors/harvard-univesity.png
- logo: ./images/sponsors/mit.png
- logo: ./images/sponsors/northeastern-university.png
- logo: ./images/sponsors/umass.png
- logo: ./images/sponsors/commonwealth-massachusetts.png
- logo: ./images/sponsors/intel.png
- logo: ./images/sponsors/red-hat.png
- logo: ./images/sponsors/futurewei.png
---

::: slot intro
### Connecting open source projects to production

OpenInfra Labs is a community, created by and for operators, testing open source code in production, publishing complete, reproducible stacks for existing & emerging workloads, to advance open source infrastructure.
:::

### Get Involved
If you are building or operating infrastructure for university or research usage, join forces with OpenInfra Labs today.

If you are a technology vendor or provider who would like to participate in OpenInfra labs, contact us at [info@openinfralabs.org](mailto:info@openinfralabs.org)

Read the [Operate First Manifesto](operate-first-manifesto.md). 

### Connect with the Community via these channels

- Freenode IRC: #openinfralabs
- Mailing list: <http://lists.opendev.org/cgi-bin/mailman/listinfo/openinfralabs>
